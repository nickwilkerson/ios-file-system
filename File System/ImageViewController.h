//
//  ImageViewController.h
//  File System
//
//  Created by Nicholas Wilkerson on 1/24/15.
//  Copyright (c) 2015 Nicholas Wilkerson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageViewController : UIViewController {
    NSString *_path;
}

-(id)initWithPath:(NSString *)path;
@property (nonatomic, readonly) NSString *path;

@end
