//
//  Folder.m
//  File System
//
//  Created by Nicholas Wilkerson on 1/24/15.
//  Copyright (c) 2015 Nicholas Wilkerson. All rights reserved.
//

#import "Folder.h"

@implementation Folder

@synthesize path=_path, contents=_contents;

- (id)initWithPath:(NSString *)path {
    self = [super init];
    if (self) {

        NSMutableArray *folderContents = [[NSMutableArray alloc] init];
        
        DIR           *d;
        struct dirent *dir;
        d = opendir([path cStringUsingEncoding:NSUTF8StringEncoding]);
        if (d)
        {
            while ((dir = readdir(d)) != NULL)
            {
                printf("%s\n", dir->d_name);
                NSString *dirName = [[NSString alloc] initWithUTF8String:dir->d_name];
                [folderContents addObject:dirName];
            }
            
            closedir(d);
        }
/*        if ([path isEqualToString:@"/"]) {
            _path = @"";
        } else {
*/            _path = path;
//        }
        _contents = folderContents;
    }
    return self;
}


@end
