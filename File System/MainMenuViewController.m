//
//  MainMenuViewController.m
//  File System
//
//  Created by Nicholas Wilkerson on 1/24/15.
//  Copyright (c) 2015 Nicholas Wilkerson. All rights reserved.
//

#import "MainMenuViewController.h"

@interface MainMenuViewController ()

@end

@implementation MainMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)openFileSystem:(id)sender {
    Folder *folder = [[Folder alloc] initWithPath:@"/"];
    FolderViewController *rootViewController = [[FolderViewController alloc] initWithStyle:UITableViewStylePlain folder:folder];
    [self.navigationController pushViewController:rootViewController animated:YES];
}


-(IBAction)openSandbox:(id)sender {
    NSString *bundlePath = [[NSBundle mainBundle] resourcePath];
    Folder *folder = [[Folder alloc] initWithPath:bundlePath];
    FolderViewController *rootViewController = [[FolderViewController alloc] initWithStyle:UITableViewStylePlain folder:folder];
    [self.navigationController pushViewController:rootViewController animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
