//
//  AppDelegate.h
//  File System
//
//  Created by Nicholas Wilkerson on 1/24/15.
//  Copyright (c) 2015 Nicholas Wilkerson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MainMenuViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    UINavigationController *navController;
}

@property (strong, nonatomic) UIWindow *window;


@end

