//
//  FolderViewController.m
//  File System
//
//  Created by Nicholas Wilkerson on 1/24/15.
//  Copyright (c) 2015 Nicholas Wilkerson. All rights reserved.
//

#import "FolderViewController.h"
@import Darwin.POSIX.sys.stat;

@interface FolderViewController ()

@end

@implementation FolderViewController

@synthesize folder=_folder;

- (id)initWithStyle:(UITableViewStyle)style folder:(Folder *)folder {
    self = [super initWithStyle:style];
    if (self) {
        _folder = folder;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = self.folder.path;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.folder.contents count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cell.textLabel.text = [self.folder.contents objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *selectedResourceName = [self.folder.contents objectAtIndex:indexPath.row];
    NSMutableString *newPath = [[NSMutableString alloc] initWithString:self.folder.path];
    if (![newPath hasSuffix:@"/"]) {
        [newPath appendString:@"/"];
    }
    [newPath appendString:selectedResourceName];
    NSLog(@"selected path: %@", newPath);
    
    struct stat s;
    
    if ([selectedResourceName isEqualToString:@"."]) {
        return;
    } else if ([selectedResourceName isEqualToString:@".."]) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    if( stat([newPath cStringUsingEncoding:NSUTF8StringEncoding],&s) == 0 )
    {
        if( s.st_mode & S_IFDIR )
        {
            Folder *newFolder = [[Folder alloc] initWithPath:newPath];
            FolderViewController *newFolderViewController = [[FolderViewController alloc] initWithStyle:UITableViewStylePlain folder:newFolder];
            [self.navigationController pushViewController:newFolderViewController animated:YES];
        }
        else if( s.st_mode & S_IFREG )
        {
            NSLog(@"found a file");
            if ([newPath hasSuffix:@".m4v"]||[newPath hasSuffix:@".mp4"]) {
                NSURL *url = [NSURL fileURLWithPath:newPath];
                MPMoviePlayerViewController *movieViewController = [[MPMoviePlayerViewController alloc] initWithContentURL:url];
               // [self.navigationController setNavigationBarHidden:YES animated:YES];
                [self.navigationController.navigationBar setTranslucent:YES];
                [self.navigationController pushViewController:movieViewController animated:YES];
            } else if ([newPath hasSuffix:@".mp3"]||[newPath hasSuffix:@".m4a"]) {
                AudioViewController *audioViewController = [[AudioViewController alloc] initWithNibName:@"AudioViewController_iPhone" bundle:nil path:newPath];
                [self.navigationController pushViewController:audioViewController animated:YES];
            } else if ([newPath hasSuffix:@".jpg"]||[newPath hasSuffix:@".png"]) {
                ImageViewController *imageViewController = [[ImageViewController alloc] initWithPath:newPath];
                [self.navigationController pushViewController:imageViewController animated:YES];
            } else {//text file
                TextViewController *textViewController = [[TextViewController alloc] initWithNibName:@"TextViewController_iPhone" bundle:nil path:newPath];
                [self.navigationController pushViewController:textViewController animated:YES];
            }
        }
        else
        {
            NSLog(@"not a file or folder");
        }
    }
    else
    {
        NSLog(@"error opening file");
    }

}




/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


@end
