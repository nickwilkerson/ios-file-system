//
//  Folder.h
//  File System
//
//  Created by Nicholas Wilkerson on 1/24/15.
//  Copyright (c) 2015 Nicholas Wilkerson. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <dirent.h>

@interface Folder : NSObject {
    NSString *_path;
    NSArray *_contents;
}

- (id)initWithPath:(NSString *)path;

@property (nonatomic) NSString *path;
@property (nonatomic, readonly) NSArray *contents;
@end
