//
//  ImageViewController.m
//  File System
//
//  Created by Nicholas Wilkerson on 1/24/15.
//  Copyright (c) 2015 Nicholas Wilkerson. All rights reserved.
//

#import "ImageViewController.h"

@interface ImageViewController ()

@end

@implementation ImageViewController

@synthesize path=_path;

-(id)initWithPath:(NSString *)path {
    self = [super init];
    if (self) {
        _path = path;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    UIImage *image = [UIImage imageWithContentsOfFile:self.path];
//    UIImageView *imageView = [[UIImageView alloc] initWithImage:image];

    CGRect rect = CGRectMake(0.0f, 0.0f, image.size.width, image.size.height);
    
    UIImageView * imageView = [[UIImageView alloc] initWithFrame:rect];
    [imageView setImage:image];
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:rect];
    [scrollView addSubview:imageView];
    self.view = scrollView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
