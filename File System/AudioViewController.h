//
//  AudioViewController.h
//  File System
//
//  Created by Nicholas Wilkerson on 1/25/15.
//  Copyright (c) 2015 Nicholas Wilkerson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface AudioViewController : UIViewController <AVAudioPlayerDelegate> {
    IBOutlet UIButton *playButton;
    IBOutlet UIButton *stopButton;
    NSString *_path;
    AVAudioPlayer *_player;
    BOOL playing;
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil path:(NSString *)path;
-(IBAction)playPause:(id)sender;
-(IBAction)stop:(id)sender;

@property (nonatomic, readonly) NSString *path;
@property (nonatomic, readonly) AVAudioPlayer *player;
@end
