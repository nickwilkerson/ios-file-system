//
//  FolderViewController.h
//  File System
//
//  Created by Nicholas Wilkerson on 1/24/15.
//  Copyright (c) 2015 Nicholas Wilkerson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "ImageViewController.h"
#import "AudioViewController.h"
#import "TextViewController.h"
#import "Folder.h"

@interface FolderViewController : UITableViewController /*<UITableViewDataDelegate>*/ {
    Folder *_folder;
}

- (id)initWithStyle:(UITableViewStyle)style folder:(Folder *)folder;
@property(nonatomic, readonly) Folder *folder;
@end
