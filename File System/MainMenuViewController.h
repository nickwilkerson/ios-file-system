//
//  MainMenuViewController.h
//  File System
//
//  Created by Nicholas Wilkerson on 1/24/15.
//  Copyright (c) 2015 Nicholas Wilkerson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FolderViewController.h"

@interface MainMenuViewController : UIViewController

-(IBAction)openFileSystem:(id)sender;
-(IBAction)openSandbox:(id)sender;

@end
