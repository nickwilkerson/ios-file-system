//
//  AudioViewController.m
//  File System
//
//  Created by Nicholas Wilkerson on 1/25/15.
//  Copyright (c) 2015 Nicholas Wilkerson. All rights reserved.
//

#import "AudioViewController.h"

@interface AudioViewController ()

@end

@implementation AudioViewController

@synthesize path=_path, player=_player;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil path:(NSString *)path {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        _path = path;
        NSURL *url = [NSURL fileURLWithPath:_path];
        NSError *error;
        _player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
        [_player setDelegate:self];
        playing = NO;
        self.title = [_path lastPathComponent];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)playPause:(id)sender {
    if (playing == NO) {
        [self.player play];
        [playButton setTitle:@"Pause" forState:UIControlStateNormal];
        stopButton.hidden = NO;
        playing = YES;
    } else {
        [self.player pause];
        [playButton setTitle:@"Play" forState:UIControlStateNormal];
        playing = NO;
    }
}

-(IBAction)stop:(id)sender {
    [self.player stop];
    self.player.currentTime = 0;
    playing = NO;
    [playButton setTitle:@"Play" forState:UIControlStateNormal];
    stopButton.hidden = YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
