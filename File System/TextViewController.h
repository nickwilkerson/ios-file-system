//
//  TextViewController.h
//  File System
//
//  Created by Nicholas Wilkerson on 1/24/15.
//  Copyright (c) 2015 Nicholas Wilkerson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextViewController : UIViewController {
    IBOutlet UITextView *textView;
    NSString *_path;
}

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil path:(NSString *)path;
-(IBAction)save:(id)sender;


@property (nonatomic) NSString *path;

@end
